<?php

namespace App\Models;


use CodeIgniter\Model;



class UserModel extends Model
{
    function getUser($email, $password)
    {
        $db = db_connect();


        $sql = "SELECT * from user WHERE email=? AND password=?";

        $query = $db->query($sql, [$email, $password]);

        //$query= $db->query("SELECT password from usuario WHERE password=?");

        $result = $query->getResult();

        return  $result;
    }
    function InsertUser($firstName, $lastName, $email, $password, $rol, $idShare)
    {
        $db = db_connect();

        $sql = "INSERT INTO user (first_name, last_name, email, password, rol,id_share,profile) VALUES ( ?, ?, ?, ?, ?,?,false)";

        $query = $db->query($sql, [$firstName, $lastName, $email, $password, $rol, $idShare]);


        $result = $query->getResult();

        return  $result;
    }

    function getOnlyUser($email)
    {
        $db = db_connect();

        $sql = 'SELECT * FROM user WHERE (email=?)';

        $query = $db->query($sql, [$email]);


        $result = $query->getResult();

        return  $result;
    }

    public  function updateProfile($idUser)
    {
        $db = db_connect();

        $sql = "SELECT profile  FROM user WHERE id=?";

        $query = $db->query($sql, [$idUser]);

        $result = $query->getResult();

        foreach ($result as $profile) {
            $result = $profile->profile;
        }

        if ($result == '0') {
            $db->query("UPDATE  user SET profile = '1'  WHERE profile = '0' AND id='$idUser'");
            $result = '1';
        } else {
            $db->query("UPDATE  user SET profile = '0'  WHERE profile = '1' AND id='$idUser'");
            $result = '0';
        }

        return $result;
    }


    public  function getProfile($idUser)
    {
        $db = db_connect();

        $sql = "SELECT profile  FROM user WHERE id=?";

        $query = $db->query($sql, [$idUser]);

        $result = $query->getResult();

        foreach ($result as $profile) {
            $result = $profile->profile;
        }
        return $result;
    }

    public  function  getOnlyPublicProfile()
    {
        $db = db_connect();

        $sql = "SELECT * FROM user WHERE profile='1'";

        $query = $db->query($sql);

        $result = $query->getResult();

        return $result;
    }

    public  function getUserById($idUser)
    {
        $db = db_connect();

        $sql = "SELECT *  FROM user WHERE id=?";

        $query = $db->query($sql, [$idUser]);

        $result = $query->getResult();

        return $result;
    }

    public function deleteResource($name, $idUser)
    {
        $db = db_connect();

        $sql = "SELECT *  FROM user WHERE id=?";

        $query = $db->query($sql, [$idUser]);
    }
}

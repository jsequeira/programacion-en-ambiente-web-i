<?php

namespace App\Models;


use CodeIgniter\Model;



class CategoryModel extends Model
{
    function getUserCategories($userId)
    {
        $db = db_connect();

        $sql = "SELECT DISTINCT category FROM user_resource WHERE user_id=?";

        $query = $db->query($sql, [$userId]);

        //$query= $db->query("SELECT password from usuario WHERE password=?");

        $result = $query->getResult();

        return  $result;

    }

    function getCategories()
    {
        $db = db_connect();

        $sql = "SELECT * FROM category";

        $query = $db->query($sql);

        //$query= $db->query("SELECT password from usuario WHERE password=?");

        $result = $query->getResult();

        return  $result;

    }
    
    
}
<?php

$isTouch = isset($mensaje);

if (!$isTouch) {
    $mensaje = '';
}


$isTouch = isset($modalInsertMjs);

if (!$isTouch) {
    $modalInsertMjs = '';
}

/* -------------------------------------------------------------------------- */
/*                              eliminar acentos                              */
/* -------------------------------------------------------------------------- */

function eliminarTildes($cadena)
{

    //Codificamos la cadena en formato utf8 en caso de que nos de errores

    //Ahora reemplazamos las letras
    $cadena = str_replace('á', 'a', $cadena);

    $cadena = str_replace(
        'é',
        'e',
        $cadena
    );

    $cadena = str_replace(
        'í',
        'i',
        $cadena
    );

    $cadena = str_replace(
        'ó',
        'o',
        $cadena
    );

    $cadena = str_replace(
        'ú',
        'u',
        $cadena
    );

    $cadena = str_replace(
        'ñ',
        'n',
        $cadena
    );
    $cadena = preg_replace(' /[^a-zA-Z0-9\_\-]+/', "", $cadena);

    return $cadena;
}



/* -------------------------------------------------------------------------- */
/*                         Session del usuario actual                         */
/* -------------------------------------------------------------------------- */


$session =  session();

$firstName = $session->firstName;
$lastName = $session->lastName;
$idUser = $session->id;





/* -------------------------------------------------------------------------- */
/*                carga las categorias del usuario sin repetir                */
/* -------------------------------------------------------------------------- */

$categoryController = new \App\Controllers\CategoryController();

$categories = $categoryController->getUserCategories($idUser);


/* -------------------------------------------------------------------------- */
/*                       carga los recursos del usuario                       */
/* -------------------------------------------------------------------------- */
$userResoursesController = new \App\Controllers\UserResourceController();

$userResourses = $userResoursesController->getUserResource($idUser);


/* -------------------------------------------------------------------------- */
/*      actualiza las noticias a la base de datos             */
/* -------------------------------------------------------------------------- */


$allNewsController = new \App\Controllers\AllNewsController();

$allNews = $allNewsController->loadNewsToDataBase($idUser);


/* -------------------------------------------------------------------------- */
/*      carga todas las noticias de la base de datos a la pagina              */
/* -------------------------------------------------------------------------- */

$allNewsController = new \App\Controllers\AllNewsController();

$allNews = $allNewsController->getAllNews($idUser);

/* -------------------------------------------------------------------------- */
/*                              carga los hashtag                             */
/* -------------------------------------------------------------------------- */

$hashtag = $allNewsController->hashtag();

/* -------------------------------------------------------------------------- */
/*                     obtiene las categorias del sistema                     */
/* -------------------------------------------------------------------------- */

$categoryController = new \App\Controllers\CategoryController();

$getCategories = $categoryController->getCategories();




// include_once 'coverfunc.php';

// session_start();

// $user = $_SESSION['user'];
// $id = $user['id'];
// $firstName = $user['first_name'];
// $lastName = $user['last_name'];

// if ($_GET['action'] == 'reload') {
//     loadNewsDataBase($id);
// }
// if ($_GET['action'] == 'load') {
//     //loadNewsDataBase($id);
// }

// $UserCategoriesnoRepeat = getUserCategoriesnoRepeat($id);

// $userResourses = getUserResources($id);


// $loadAllNewstoPage = loadAllNewstoPage($id);

/* function searchResourse()
{
    session_start();
    $user = $_SESSION['user'];
    $id = $user['id'];
    $userRecourses = getUserRecourses($id);
    foreach ($userRecourses as $key) {
        asdas
}
 */
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    <link rel="stylesheet" href="<?php echo base_url('css/cover.css') ?>" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" />
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
    <!-- MDB -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.css" rel="stylesheet" />

</head>

<body>

    <!-- ----------------------------------------------------------------------- -->
    <!--                                 HEADER                                  -->
    <!-- ----------------------------------------------------------------------- -->

    <header>
        <nav class="navbar navbar-expand-md navbar-light bg-light border-bottom">
            <a class="navbar-brand" href="<?php echo base_url('index') ?>">
                <img src="<?php echo base_url('img/logo2.png') ?>" width="130" height="70" class="d-inline-block align-top" alt="" />
            </a>

            <div class="collapse navbar-collapse justify-content-end " id="navbarNav">
                <form class="d-flex" method="get" action="/AllNewsController/search">
                    <div class="form-outline">
                        <input type="search" id="form1" name="word" class="form-control" />
                        <label class="form-label" for="form1">Search</label>
                    </div>
                    <button type="submit" class="btn btn-primary me-5">
                        <i class="fas fa-search"></i>
                    </button>
                </form>
            </div>


            <ul class="navbar-nav  me-5">
                <!-- Notification dropdown -->

                <div class="form-check form-switch">

                    <?php if ($profile == '1') : ?>
                        <a href="/userController/updateProfile/<?php echo $idUser ?>">
                            <input class="form-check-input" type="checkbox" id="flexSwitchCheckChecked" checked />
                        </a>
                        <label class="form-check-label" for="flexSwitchCheckChecked">Public profile</label>
                    <?php else : ?>
                        <a href="/userController/updateProfile/<?php echo $idUser ?>">
                            <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault" />
                        </a>
                        <label class="form-check-label" for="flexSwitchCheckDefault">Public profile</label>
                    <?php endif; ?>

                </div>

                <!-- Notification dropdown -->
            </ul>
            <ul class="navbar-nav me-5">


                <h7>
                    <?php echo $firstName . ' ' . $lastName; ?>
                </h7>


            </ul>
            <ul class="navbar-nav me-5">

                <a href="/index/logout">
                    <button type=" button" class="btn btn-outline-secondary">
                        <i class="fas fa-sign-out-alt"></i>
                    </button>
                </a>

            </ul>

        </nav>
    </header>

    <!-- ----------------------------------------------------------------------- -->
    <!--                                  MAIN                                   -->
    <!-- ----------------------------------------------------------------------- -->

    <main>

        <!-- Modal update  -->
        <div class="modal fade" id="modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit resource</h5>
                        <button type="button" class="btn-close" data-mdb-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">

                        <form method="Get" action="/UserResourceController/updateResource">
                            <input hidden type="text" class="form-control" name="idUser" value="<?php echo $idUser ?>" />
                            <div class="form-outline ">
                                <input type="text" id="modalName" name='name' class="form-control" />
                                <input hidden type="text" id="modalOldName" name='oldName' class="form-control" />
                                <label class="form-label" for="form1">Name</label>
                            </div>
                            <div class="form-outline mt-3">
                                <!-- <input type="text" id="modalCategory" name='category' class="form-control" />
                              
                                <label class="form-label" for="form1">Category</label> -->

                                <input hidden type="text" id="modalOldCategory" name='oldCategory' class="form-control" />

                                <select name='category' class="form-select me-1" aria-label="Default select example" ;>
                                    <option type="text" id="modalCategory" ?></option>
                                    <?php
                                    foreach ($getCategories as $category) :
                                    ?>
                                        <option><?php echo $category->name ?></option>

                                    <?php endforeach ?>

                                </select>


                            </div>
                            <div class="form-outline mt-3">
                                <input type="text" id="modalUrl" name='url' class="form-control" />
                                <input hidden type="text" id="modalOldUrl" name='oldUrl' class="form-control" />
                                <label class="form-label" for="form1">Url</label>
                            </div>
                            <div class="mt-3">
                                <button type="submit" class="btn btn-primary">Save changes</button>

                                <button type="button" class="btn btn-secondary" data-mdb-dismiss="modal">
                                    Close
                                </button>
                            </div>
                        </form>

                    </div>




                </div>
            </div>
        </div>
        <!-- Modal insert -->
        <div class="modal fade" id="modalinsert" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel2">New resource</h5>
                        <button type="button" class="btn-close" data-mdb-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">

                        <form method="Get" action="/UserResourceController/insertUserResource">
                            <input hidden type="text" class="form-control" name="idUser" value="<?php echo $idUser ?>" />
                            <div class="form-outline ">
                                <input type="text" name='name' class="form-control" />

                                <label class="form-label" for="form1">Name</label>
                            </div>
                            <div class="form-outline mt-3">

                                <select name='category' class="form-select me-1" aria-label="Default select example" ;>
                                    <option type="text" id="modalCategory" ?>Select category</option>
                                    <?php
                                    foreach ($getCategories as $category) :
                                    ?>
                                        <option><?php echo $category->name ?></option>

                                    <?php endforeach ?>

                                </select>
                            </div>
                            <div class="form-outline mt-3">
                                <input type="text" name='url' class="form-control" />

                                <label class="form-label" for="form1">Url</label>
                            </div>
                            <div class="mt-3">
                                <button type="submit" class="btn btn-primary">Save changes</button>

                                <button type="button" class="btn btn-secondary" data-mdb-dismiss="modal">
                                    Close
                                </button>
                            </div>
                        </form>

                    </div>




                </div>
            </div>
        </div>
        <div class="container-tab rounded border mt-4 mb-4">

            <ul class="nav nav-tabs nav-fill mb-3 border-bottom" id="ex1" role="tablist">
                <?php
                $numero = 1;
                foreach ($categories  as $userCategory) :
                ?>
                    <?php
                    if ($numero == 1) :
                    ?>
                        <li class="nav-item " role="presentation">
                            <a class="nav-link active" id="myResourses1" data-mdb-toggle="tab" href="#myResourses" role="tab" aria-controls="ex2-tabs-1" aria-selected="true">My resourses</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link" id="main1" data-mdb-toggle="tab" href="#main" role="tab" aria-controls="ex2-tabs-2" aria-selected="false">Main</a>
                        </li>
                        <?php
                        $numero = 2;
                        ?>
                    <?php
                    endif;
                    ?>

                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="<?php echo $userCategory->category ?>1" data-mdb-toggle="tab" href="#<?php echo $userCategory->category ?>" role="tab" aria-controls="ex2-tabs-2" aria-selected="false"><?php echo $userCategory->category ?></a>
                    </li>
                <?php
                endforeach;
                ?>

            </ul>
            <!-- Tabs navs -->

            <!-- Tabs content -->
            <div class="tab-content" id="ex2-content">
                <div class="tab-pane fade show active" id="myResourses" role="tabpanel">

                    <div class="float-start d-flex flex-column ms-3">

                        <button type="button" data-mdb-toggle="modal" data-mdb-target="#modalinsert" class="btn btn-secondary ms-3 me-5 mt-1 w-75">
                            New Resource
                        </button>

                        <a href="/userController/getOnlyPublicProfile">
                            <button type="button" class="btn btn-warning ms-3 me-5 mt-1 w-75">
                                Public resources
                            </button>
                        </a>
                    </div>

                    <div data-mdb-spy="scroll" data-mdb-target="#scrollspy1" data-mdb-offset="0" class="scrollspy-example ">
                        <table class="table align-middle border rounded-top  border-secondary">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Category</th>
                                    <th scope="col">Watch</th>
                                    <th scope="col">Edit</th>
                                    <th scope="col">Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $num = 0;
                                foreach ($userResourses as $resourse) :
                                ?>

                                    <?php
                                    $num = $num + 1;
                                    ?>
                                    <tr>
                                        <th scope="row"> <?php echo $num ?></th>
                                        <td scope="row">
                                            <a><?php echo $resourse->name ?></a>
                                        </td>
                                        <td>
                                            <a><?php echo $resourse->category ?></a>
                                        </td>
                                        <td>
                                            <?php
                                            $urlChanged = str_replace(".", "}", "$resourse->url");
                                            $url = str_replace("/", ".", "$urlChanged ");
                                            ?>

                                            <a href="/AllNewsController/loadAllNewsByResource/<?php echo $idUser ?>/<?php echo  $url  ?>">
                                                <button type="button" class="btn btn-secondary btn-sm px-3">
                                                    <i class="fas fa-eye"></i>
                                                </button>
                                            </a>


                                        </td>
                                        <td>
                                            <a>
                                                <input hidden id="<?php echo  $resourse->name, 'name'; ?>" value='<?php echo $resourse->name ?>'></label>
                                                <input hidden id="<?php echo $resourse->name, 'url' ?>" value='<?php echo $resourse->url ?>'></label>
                                                <input hidden id="<?php echo $resourse->name, 'category' ?>" value='<?php echo $resourse->category ?>'></label>

                                                <button id="<?php echo $resourse->name ?>" onclick="modalFunction(this.id)" type="button" data-mdb-toggle="modal" data-mdb-target="#modal" class="btn btn-secondary btn-sm px-3">
                                                    <i class="fas fa-edit"></i>
                                                </button>

                                            </a>

                                        </td>
                                        <td>

                                            <a href="/UserResourceController/deleteResource/<?php echo $resourse->name ?>/<?php echo $idUser ?>">
                                                <button type="button" class="btn btn-secondary btn-sm px-3">
                                                    <i class="fas fa-eraser"></i>
                                                </button>
                                            </a>

                                        </td>
                                    </tr>


                                <?php
                                endforeach
                                ?>
                            </tbody>
                        </table>


                    </div>
                </div>



                <div class="tab-pane fade " id="main" role="tabpanel">

                    <div class="container float-end ">
                        <?php foreach ($hashtag as $hash) : ?>
                            <a href="/AllNewsController/loadHashtag/<?php echo eliminarTildes($hash->categories)  ?>">
                                <button type="button" class="btn btn-outline-secondary btn-rounded mb-2 btn-sm"><?php echo '#' . eliminarTildes($hash->categories) ?></button>
                            </a>
                        <?php endforeach; ?>
                    </div>

                    <div class="float-start d-flex flex-column ms-3 mt-5">

                        <button type="button" data-mdb-toggle="modal" data-mdb-target="#modalinsert" class="btn btn-secondary ms-3 me-5 mt-1 w-75">
                            New Resource
                        </button>

                        <a href="/userController/getOnlyPublicProfile">
                            <button type="button" class="btn btn-warning ms-3 me-5 mt-1 w-75">
                                Public resources
                            </button>
                        </a>
                    </div>


                    <div data-mdb-spy="scroll" data-mdb-target="#scrollspy1" data-mdb-offset="0" class="scrollspy-example">
                        <?php
                        foreach ($allNews as $new) :
                        ?>
                            <div class="card border mb-1">

                                <?php
                                if (substr($new->img_url, -3) == 'mp4') :
                                ?>
                                    <img src="http://rafikisafari.com/wp/wp-content/themes/salient/img/no-video-img.png" class="card-img-top  mt-3 ms-4 w-25 h-25" alt="..." />
                                <?php
                                else :
                                ?>
                                    <img src="<?php echo $new->img_url ?>" class="card-img-top  mt-3 ms-4 w-25 h-25" alt="..." />
                                <?php
                                endif;
                                ?>
                                <div class="card-body mt-1 ms-1">
                                    <h5 class="card-title"> <?php echo $new->title ?></h5>
                                    <h6 class="card-title">Category: <?php echo $new->categories ?></h6>
                                    <h7 class="card-title">Source: <?php echo $new->source_name ?></h7>
                                    <br>
                                    <h8 class="card-title">Date: <?php echo $new->date ?></h8>
                                    <p class="card-text">
                                        <?php echo $new->description ?>
                                    </p>
                                    <a href="<?php echo $new->permalink ?>" class="btn btn-secondary">Visit Website</a>
                                </div>
                            </div>



                        <?php
                        endforeach;
                        ?>

                    </div>

                </div>

                <?php
                foreach ($categories  as $userCategory) :

                ?>

                    <div class="tab-pane fade" id="<?php echo $userCategory->category ?>" role="tabpanel">

                        <div class="container float-end ">
                            <?php foreach ($hashtag as $hash) : ?>
                                <a href="/AllNewsController/loadHashtag/<?php echo eliminarTildes($hash->categories) ?>">
                                    <button type="button" class="btn btn-outline-secondary btn-rounded mb-2 btn-sm"><?php echo '#' . eliminarTildes($hash->categories) ?></button>
                                </a>
                            <?php endforeach; ?>
                        </div>

                        <div class="float-start d-flex flex-column ms-3 mt-5">

                            <button type="button" data-mdb-toggle="modal" data-mdb-target="#modalinsert" class="btn btn-secondary ms-3 me-5 mt-1 w-75">
                                New Resource
                            </button>

                            <a href="/userController/getOnlyPublicProfile">
                                <button type="button" class="btn btn-warning ms-3 me-5 mt-1 w-75">
                                    Public resources
                                </button>
                            </a>
                        </div>

                        <div data-mdb-spy="scroll" data-mdb-target="#scrollspy1" data-mdb-offset="0" class="scrollspy-example">

                            <?php
                            $newsByCategory = $allNewsController->getAllNewsByCategory($idUser, $userCategory->category);
                            ?>

                            <?php
                            foreach ($newsByCategory as $new) :
                            ?>

                                <div class="card border mb-1">
                                    <?php
                                    if (substr($new->img_url, -3) == 'mp4') :
                                    ?>
                                        <img src="http://rafikisafari.com/wp/wp-content/themes/salient/img/no-video-img.png" class="card-img-top  mt-3 ms-4 w-25 h-25" alt="..." />
                                    <?php
                                    else :
                                    ?>
                                        <img src="<?php echo $new->img_url ?>" class="card-img-top  mt-3 ms-4 w-25 h-25" alt="..." />
                                    <?php
                                    endif;
                                    ?>
                                    <div class="card-body mt-1 ms-1">
                                        <h5 class="card-title"> <?php echo $new->title ?></h5>
                                        <h6 class="card-title">Category: <?php echo $new->categories ?></h6>
                                        <h7 class="card-title">Source: <?php echo $new->source_name ?></h7>
                                        <br>
                                        <h8 class="card-title">Date: <?php echo $new->date ?></h8>
                                        <p class="card-text">
                                            <?php echo $new->description ?>
                                        </p>
                                        <a href="<?php echo $new->permalink ?>" class="btn btn-secondary">Visit Website</a>
                                    </div>
                                </div>

                            <?php
                            endforeach;
                            ?>
                        </div>
                    </div>
                <?php
                endforeach;
                ?>
            </div>


        </div>

    </main>

    <!-- ----------------------------------------------------------------------- -->
    <!--                                 FOOTER                                  -->
    <!-- ----------------------------------------------------------------------- -->

    <footer class="text-center text-white" style="background-color:#E0E0E0">
        <!-- Grid container -->
        <div class="container p-4"></div>
        <!-- Grid container -->

        <!-- Copyright -->
        <div class="text-white p-3" style="background-color: #757575">
            © 2020 Copyright:
            <a class="text-white" href="https://mdbootstrap.com/">Proyecto web I</a>
        </div>
        <!-- Copyright -->
    </footer>
    <!-- MDB -->
    <!-- ----------------------------------------------------------------------- -->
    <!--                               JAVASCRIPH                                -->
    <!-- ----------------------------------------------------------------------- -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


    <script type="text/javascript" src="<?php echo base_url('javascript/cover.js') ?>"> </script>

    <?php
    if (!$modalInsertMjs == '') {
        echo "<script type='text/javascript'>alert('$modalInsertMjs');</script>";
    } ?>
    <!-- ----------------------------------------------------------------------- -->
    <!--                               JAVASCRIPH                                -->
    <!-- ----------------------------------------------------------------------- -->

</body>


</html>
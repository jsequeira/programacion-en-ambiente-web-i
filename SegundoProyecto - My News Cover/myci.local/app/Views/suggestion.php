<?php
// include_once 'suggestionfunc.php';

// $categories = getCategories();
// session_start();
// $user = $_SESSION['user'];
// $firstName = $user['first_name'];
// $lastName = $user['last_name'];

// if ($_GET) {

//   $url = $_GET['url'];

//   $idUser = $user['id'];

//   $resourcename = $_GET['resourcename'];

//   $category = $_GET['category'];

//   insertResources($idUser, $url, $resourcename, $category);
//  header('location:cover.php?action=load');

// }

/* -------------------------------------------------------------------------- */
/*                    Iniciar la sesion y cargar los datos                    */
/* -------------------------------------------------------------------------- */

$session =  session();
$firstName = $session->firstName;
$lastName = $session->lastName;
$idUser = $session->id;

/* -------------------------------------------------------------------------- */
/*                        obtener todas las categorias                        */
/* -------------------------------------------------------------------------- */

$categoryController = new \App\Controllers\CategoryController();

$categories = $categoryController->getCategories();


?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Document</title>
  <link rel="stylesheet" href="suggestions.css" />

  <link rel="stylesheet" href="<?php echo base_url('css/suggestion.css') ?>" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" />
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
  <!-- MDB -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.css" rel="stylesheet" />


</head>

<body>

  <!-- ----------------------------------------------------------------------- -->
  <!--                                 HEADER                                  -->
  <!-- ----------------------------------------------------------------------- -->

  <header>
    <nav class="navbar navbar-expand-md navbar-light bg-light border-bottom">
      <a class="navbar-brand" href="index.php">
        <img src="<?php echo base_url('img/logo2.png') ?>" width="130" height="70" class="d-inline-block align-top" alt="" />
      </a>
      <div class="collapse navbar-collapse justify-content-end " id="navbarNav">
        <ul class="navbar-nav me-5">

          <div class="btn-group me-4">
            <button type="button" class="btn btn-outline-secondary  dropdown-toggle" data-mdb-toggle="dropdown" data-mdb-display="static" aria-expanded="false">
              <?php echo $firstName;
              echo ' ';
              echo $lastName ?>
            </button>
            <ul class="dropdown-menu dropdown-menu-end dropdown-menu-lg-start ">
              <li><a class="dropdown-item text-center " href="index.php?action=logout">Log Out</a></li>

            </ul>

          </div>

        </ul>
      </div>
    </nav>
  </header>


  <!-- ----------------------------------------------------------------------- -->
  <!--                                  MAIN                                   -->
  <!-- ----------------------------------------------------------------------- -->

  <main class="d-flex flex-row justify-content-center  align-items-center">

    <div class="container-form mt-4 mb-4 ">
      <form method="GET" action="/UserResourceController/insertUserResource" class="border p-5 border-secondary rounded">

        <p class="text-center text-primary"><?php echo 'Add the first feed (optional)' ?></p>
        <!-- idUser input -->
        <input hidden type="text" id="form3Example3" class="form-control" name="idUser" value="<?php echo $idUser ?>" />
        <!-- Resource Name input -->
        <div class="form-outline mb-4">
          <input type="text" id="form3Example3" class="form-control" name="name" required="required" />
          <label class="form-label" for="form3Example3">Name</label>
        </div>

        <!-- Url input -->
        <div class="form-outline mb-4 ">
          <input id="inputrss" type="text" class="form-control" name="url" minlength="8" value="rsslink" required="required" />
          <label id="labelrss" class="form-label" for="form3Example4">RSS</label>
        </div>
        <div class="mb-4">
          <select class="form-select" aria-label="Default select example" name="category" required="required">
            <option selected>Select category resource</option>
            <?php
            foreach ($categories as $category) :
            ?>
              <option name="category">
                <?php echo $category->name ?>
              </option>
            <?php
            endforeach
            ?>

          </select>
        </div>
        <!-- Submit button -->
        <button type="submit" class="btn btn-secondary btn-block mb-3">Save</button>

        <!-- Back buttons -->
        <a href="/userController/redirectCover/<?php echo $idUser  ?>">
          <button type="button" class="btn btn-warning btn-block">Skip</button>
        </a>
      </form>
    </div>

    <div class=" ms-5 ">
      <table class="table align-middle">
        <h1 class="text-center">Popular RSS</h1>
        <thead>
          <tr>
            <th scope="col">Name</th>
            <th scope="col">Add RSS</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Elpais</td>
            <td>
              <button id="#https://feeds.elpais.com/mrss-s/pages/ep/site/elpais.com/portada" type="button" onclick="cambiarRss(this.id)" class="btnicon btn-warning btn-sm px-3">
                <i id="https://feeds.elpais.com/mrss-s/pages/ep/site/elpais.com/portada" class="icon fas fa-times"></i>
              </button>
            </td>
          </tr>
          <tr>
            <td>Crhoy</td>
            <td>
              <button id="#https://feeds.feedburner.com/crhoy/wSjk" type="button" onclick="cambiarRss(this.id)" class="btnicon btn-warning btn-sm px-3">
                <i id="https://feeds.feedburner.com/crhoy/wSjk" class="icon fas fa-times"></i>
              </button>
            </td>
          </tr>
        </tbody>
      </table>
    </div>

  </main>

  <!-- ----------------------------------------------------------------------- -->
  <!--                                 FOOTER                                  -->
  <!-- ----------------------------------------------------------------------- -->

  <footer class="text-center text-white" style="background-color:#E0E0E0">
    <!-- Grid container -->
    <div class="container p-4"></div>
    <!-- Grid container -->

    <!-- Copyright -->
    <div class="text-white p-3" style="background-color: #757575">
      © 2020 Copyright:
      <a class="text-white" href="https://mdbootstrap.com/">Proyecto web I</a>
    </div>
    <!-- Copyright -->
  </footer>
  <!-- MDB -->

  <!-- ----------------------------------------------------------------------- -->
  <!--                               JAVASCRIPH                                -->
  <!-- ----------------------------------------------------------------------- -->

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.js"></script>
  <script src="<?php echo base_url('javascript/suggestion.js') ?>"></script>
  <!-- ----------------------------------------------------------------------- -->
  <!--                               JAVASCRIPH                                -->
  <!-- ----------------------------------------------------------------------- -->

</body>

</html>
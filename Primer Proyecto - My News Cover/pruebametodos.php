
<?php



function conexionCover()
{
    $link = 'mysql:host=localhost;dbname=cover';
    $usuario = 'root';
    $pass = 'root';

    try {

        $pdo = new PDO($link, $usuario, $pass);
        echo 'Conectado<br>';
    } catch (PDOException $e) {
        print "Error!" . $e->getMessage() . "<br>";
        die();
    }
    return $pdo;
}

function getAllResourcesUrl($userId)
{

    $query = 'SELECT url,category FROM user_resource WHERE user_id = ?';
    $gsent = conexionCover()->prepare($query);
    $gsent->execute(array($userId));

    $result = $gsent->fetchAll();

    return $result;

    var_dump($result);
}


function deleteOldNews($userId)
{
    $query = 'DELETE  FROM all_news WHERE id_user= ?';
    $gsent = conexionCover()->prepare($query);
    $gsent->execute(array($userId));

    $result = $gsent->fetchAll();

    return $result;
}
function convertDateTomySqlFormat($str)
{
    return date('Y-m-d H:i:s', strtotime($str));
}

function insertNews($userId, $category, $resource, $title, $description, $permalink, $date, $img_url, $categories)
{
    $query = 'INSERT INTO all_news (id_user, category, resource, title, description, permalink, date, img_url, categories) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);';
    $addSentence = conexionCover()->prepare($query);
    $addSentence->execute(array($userId,$category,$resource.$resource,$title,$description,$permalink,$date,$img_url,$categories));

}

function loadNewsDataBase($userId)
{

    $urls = getAllResourcesUrl($userId);

    deleteOldNews($userId);

    foreach ($urls as $urlkey) {
        $i = 0;
        $urlResourse =  $urlkey['url'];
        $categoryUser = $urlkey['category'];
        $rss = simplexml_load_file($urlResourse);
        foreach ($rss->channel->item as $item) {
            $link = $item->link;  //extrae el Permanklink
            $title = $item->title;  //extrae el titulo
            $date = $item->pubDate;  //extrae la fecha

            $url_imagen = $item->enclosure['url']; //extrae el link de la imagen 
            if (!$url_imagen) {
                $url_imagen = $item->children('media', true)->content->attributes(); //extrae link de imagen
            }
           
            $categoryNew = $item->category; //extrae categoria
            $guid = $item->guid;//extrae LINK noticia original categoria
            $dateMySqlFormat = convertDateTomySqlFormat($date);// convierte la fecha al formato mySql
            $description = strip_tags($item->description);
            echo $dateMySqlFormat;
            echo $date;

            insertNews($userId,$categoryUser,$urlResourse ,$title,$description, $guid,$dateMySqlFormat, $url_imagen,$categoryNew); 



            /* $description = strip_tags($item->description);  //extrae la descripcion
            if (strlen($description) > 400) { //limita la descripcion a 400 caracteres
                $stringCut = substr($description, 0, 200);
                $description = substr($stringCut, 0, strrpos($stringCut, ' ')) . '...';
            }
            if ($i < 16) { // extrae solo 16 items
                echo '<div class="cuadros1"><h4><a href="' . $link . '" target="_blank">' . $title . '</a></h4><br><img src="' . $url_imagen . '"><br>' . $description . '<br><div class="time">' . $date . '</div></div>';
            }
            $i++; */
        }
    }



    return $urls;
}
?>
<?php
include_once 'functions.php';

/* -------------------------------------------------------------------------- */
/*                           INSERT INTO USER TABLE                           */
/* -------------------------------------------------------------------------- */
if ($_GET) {
  $email = $_GET["email"];
  $user = getOnlyUser($email);

  if ($user) {

    header("location:register.php?action=message");
  } else {

    $firstName = $_GET["firstname"];
    $lastName = $_GET["lastname"];
    $email = $_GET["email"];
    $password = $_GET["password"];
    echo $firstName, $lastName, $email, $password;
    $sql_agregar = "INSERT INTO user (id, first_name, last_name, email, password, rol) VALUES (NULL, ?, ?, ?, ?, ?)";
    $sentencia_agregar = conexionCover()->prepare($sql_agregar);
    $state = $sentencia_agregar->execute(array($firstName, $lastName, $email, $password, "user"));

    echo var_dump($state);
    if ($state) {
      header("Location:login.php");
      echo 'agregado';
    }
  }
}



?>


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Document</title>
  <link rel="stylesheet" href="register.css" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" />
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
  <!-- MDB -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.css" rel="stylesheet" />
</head>

<body>

  <!-- ----------------------------------------------------------------------- -->
  <!--                                 HEADER                                  -->
  <!-- ----------------------------------------------------------------------- -->


  <header>
    <nav class="navbar navbar-expand-md navbar-light bg-light border-bottom">
      <a class="navbar-brand" href="index.php">
        <img src="img/logo2.png" width="130" height="70" class="d-inline-block align-top" alt="" />
      </a>
      <div class="collapse navbar-collapse justify-content-end " id="navbarNav">
        <ul class="navbar-nav me-5">

          <div class="btn-group me-4">
            <a href="login.php">
              <button type="button" class="btn btn-outline-secondary " data-mdb-display="static" aria-expanded="false">
                Log in
              </button>
            </a>

          </div>

        </ul>
      </div>
    </nav>
  </header>

  <!-- ----------------------------------------------------------------------- -->
  <!--                                  MAIN                                   -->
  <!-- ----------------------------------------------------------------------- -->

  <main>
    <div class="container-form mt-2 mb-2">
      <?php
      if ($_GET['action'] == 'message') :
      ?>
        <p class="text-center text-danger"><?php echo 'This email is already registered' ?></p>
      <?php endif ?>

      <form method="GET" class="border p-5 border-secondary rounded">
        <!-- 2 column grid layout with text inputs for the first and last names -->
       


          <!-- first input -->
          <div class="form-outline mb-4">
            <input type="text" id="form3Example1" class="form-control" name="firstname" required="required" />
            <label class="form-label" for="form3Example3">First name</label>
          </div>



          <!-- Lastname input -->
          <div class="form-outline mb-4">
            <input type="text" id="form3Example2" class="form-control" name="lastname" required="required" />
            <label class="form-label" for="form3Example3">Last name</label>
          </div>



      

        <!-- Email input -->
        <div class="form-outline mb-4">
          <input type="email" id="form3Example3" class="form-control" name="email" required="required" />
          <label class="form-label" for="form3Example3">Email address</label>
        </div>

        <!-- Password input -->
        <div class="form-outline mb-4">
          <input type="password" id="form3Example4" class="form-control" name="password" minlength="8" required="required" />
          <label class="form-label" for="form3Example4">Password</label>
          <input name="action" value="register" hidden />
        </div>
        <div class="col">
          <!-- Simple link -->
          <p class="text-center">if you have account,
            <a text-center href="login.php">log in here</a>
          </p>
        </div>
        <!-- Submit button -->
        <button type="submit" class="btn btn-secondary btn-block mb-4">Register</button>
      </form>
    </div>
  </main>

  <!-- ----------------------------------------------------------------------- -->
  <!--                                 FOOTER                                  -->
  <!-- ----------------------------------------------------------------------- -->

  <footer class="bg-light text-center text-lg-start">
    <!-- Grid container -->
    <div class="container p-4">
      <!--Grid row-->
      <div class="row">
        <!--Grid column-->
        <div class="col-lg-6 col-md-12 mb-4 mb-md-0">
          <h5 class="text-uppercase">About</h5>

          <p>
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Iste atque
            ea quis molestias. Fugiat pariatur maxime quis culpa corporis vitae
            repudiandae aliquam voluptatem veniam, est atque cumque eum delectus
            sint!
          </p>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
          <h5 class="text-uppercase">Devices</h5>

          <ul class="list-unstyled mb-0">
            <li>
              <a href="#!" class="text-dark">PC</a>
            </li>
            <li>
              <a href="#!" class="text-dark">iOS</a>
            </li>
            <li>
              <a href="#!" class="text-dark">Android</a>
            </li>

          </ul>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
          <h5 class="text-uppercase mb-0">Social media</h5>

          <ul class="list-unstyled">
            <li>
              <a href="#!" class="text-dark">Fcebook</a>
            </li>
            <li>
              <a href="#!" class="text-dark">twitter</a>
            </li>
            <li>
              <a href="#!" class="text-dark">Diaspora</a>
            </li>
          </ul>
        </div>
        <!--Grid column-->
      </div>
      <!--Grid row-->
    </div>
    <!-- Grid container -->

    <!-- Copyright -->
    <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2)">
      © 2020 Copyright:
      <a class="text-dark" href="https://mdbootstrap.com/">MDBootstrap.com</a>
    </div>
    <!-- Copyright -->
  </footer>
  <!-- MDB -->

  <!-- ----------------------------------------------------------------------- -->
  <!--                               JAVASCRIPH                                -->
  <!-- ----------------------------------------------------------------------- -->

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.js"></script>

  <!-- ----------------------------------------------------------------------- -->
  <!--                               JAVASCRIPH                                -->
  <!-- ----------------------------------------------------------------------- -->

</body>

</html>
<?php
include_once 'newresourcefunc.php';
$categories = getCategories();

session_start();
$session = $_SESSION["user"];
$id = $session["id"];
$url = $_GET["url"];
echo $id;
echo $url;
$resource=getOnlyResourse($id,$url);

if ($resource) {
   header('location:newresource.php?action=message');
}else{
  if ($_GET['action']=='insert') {

    $idUser = $session["id"];
  
    $resourcename = $_GET["resourcename"];
  
    $category = $_GET["category"];
  
    insertResources($idUser, $url, $resourcename, $category);
    header('location:cover.php?action=load');
  }
}


  // if (getOnlyResourse($id, $url)) {
  //   echo '1ssss';
  //   $idUser = $session["id"];

  //   $resourcename = $_GET["resourcename"];

  //   $category = $_GET["category"];

  //   insertResources($idUser, $url, $resourcename, $category);

  //   // header('location:cover.php?action=load');
  // }



?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Document</title>
  <link rel="stylesheet" href="newrecourse.css" />

  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" />
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
  <!-- MDB -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.css" rel="stylesheet" />


</head>

<body>

  <!-- ----------------------------------------------------------------------- -->
  <!--                                 HEADER                                  -->
  <!-- ----------------------------------------------------------------------- -->

  <header>
    <nav class="navbar navbar-expand-md navbar-light bg-light border-bottom">
      <a class="navbar-brand" href="index.php">
        <img src="img/logo2.png" width="130" height="70" class="d-inline-block align-top" alt="" />
      </a>
      <div class="collapse navbar-collapse justify-content-end " id="navbarNav">
        <ul class="navbar-nav me-5">

          <div class="btn-group me-4">
            <button type="button" class="btn btn-outline-secondary  dropdown-toggle" data-mdb-toggle="dropdown" data-mdb-display="static" aria-expanded="false">
              <?php echo $session['first_name'];
              echo ' ';
              echo $session['last_name']; ?>
            </button>
            <ul class="dropdown-menu dropdown-menu-end dropdown-menu-lg-start ">
              <li><a class="dropdown-item text-center " href="newresource.php">New Resource</a></li>
              <li><a class="dropdown-item text-center " href="index.php?action=logout">Log Out</a></li>

            </ul>

          </div>

        </ul>
      </div>
    </nav>
  </header>

  <!-- ----------------------------------------------------------------------- -->
  <!--                                  MAIN                                   -->
  <!-- ----------------------------------------------------------------------- -->

  <main>
    <div class="container-form mt-4 mb-4">
      <form method="GET" class="border p-4 border-secondary rounded">
        <?php
        if ($_GET['action'] == 'message') :
        ?>
          <p class="text-center text-danger"><?php echo 'This resource already exists' ?></p>
        <?php endif ?>
        <!-- Resource Name input -->
        <div class="form-outline mb-4">
          <input type="text" id="form3Example3" class="form-control" name="resourcename" required="required" />
          <label class="form-label" for="form3Example3">Name</label>
        </div>

        <!-- Url input -->
        <div class="form-outline mb-4">
          <input type="text" id="form3Example4" class="form-control" name="url" minlength="8" required="required" />
          <input hidden value="insert" type="text" id="form3Example4" class="form-control" name="action" minlength="8" required="required" />
          <label class="form-label" for="form3Example4">RSS URL</label>
        </div>
        <div class="mb-4">
          <select class="form-select" aria-label="Default select example" name="category" required="required">
            <option selected>
              <p class="fs-4">select category resource</p>
            </option>
            <?php
            foreach ($categories as $category) :
            ?>
              <option name="category">
                <?php echo $category['name'] ?>
              </option>
            <?php
            endforeach
            ?>

          </select>
        </div>
        <!-- Submit button -->
        <button type="submit" class="btn btn-secondary btn-block mb-3">Save</button>

        <!-- Back buttons -->
        <a href="cover.php">
          <button type="button" class="btn btn-warning btn-block">Back</button>
        </a>
      </form>
    </div>
  </main>

  <!-- ----------------------------------------------------------------------- -->
  <!--                                 FOOTER                                  -->
  <!-- ----------------------------------------------------------------------- -->

  <footer class="bg-light text-center text-lg-start">
    <!-- Grid container -->
    <div class="container p-4">
      <!--Grid row-->
      <div class="row">
        <!--Grid column-->
        <div class="col-lg-6 col-md-12 mb-4 mb-md-0">
          <h5 class="text-uppercase">About</h5>

          <p>
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Iste atque
            ea quis molestias. Fugiat pariatur maxime quis culpa corporis vitae
            repudiandae aliquam voluptatem veniam, est atque cumque eum delectus
            sint!
          </p>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
          <h5 class="text-uppercase">Devices</h5>

          <ul class="list-unstyled mb-0">
            <li>
              <a href="#!" class="text-dark">PC</a>
            </li>
            <li>
              <a href="#!" class="text-dark">iOS</a>
            </li>
            <li>
              <a href="#!" class="text-dark">Android</a>
            </li>

          </ul>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
          <h5 class="text-uppercase mb-0">Social media</h5>

          <ul class="list-unstyled">
            <li>
              <a href="#!" class="text-dark">Fcebook</a>
            </li>
            <li>
              <a href="#!" class="text-dark">twitter</a>
            </li>
            <li>
              <a href="#!" class="text-dark">Diaspora</a>
            </li>
          </ul>
        </div>
        <!--Grid column-->
      </div>
      <!--Grid row-->
    </div>
    <!-- Grid container -->

    <!-- Copyright -->
    <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2)">
      © 2020 Copyright:
      <a class="text-dark" href="https://mdbootstrap.com/">MDBootstrap.com</a>
    </div>
    <!-- Copyright -->
  </footer>
  <!-- MDB -->

  <!-- ----------------------------------------------------------------------- -->
  <!--                               JAVASCRIPH                                -->
  <!-- ----------------------------------------------------------------------- -->

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.js"></script>

  <!-- ----------------------------------------------------------------------- -->
  <!--                               JAVASCRIPH                                -->
  <!-- ----------------------------------------------------------------------- -->

</body>

</html>
<?php

function conexionCover()
{
  $link = 'mysql:host=localhost;dbname=cover';
  $usuario = 'root';
  $pass = '12345678';

  try {

    $pdo = new PDO($link, $usuario, $pass);
  } catch (PDOException $e) {
    print "Error!" . $e->getMessage() . "<br>";
    die();
  }
  return $pdo;
}


function getCategories()
{

  $query = 'SELECT * FROM category';
  $gsent = conexionCover()->prepare($query);
  $gsent->execute();

  $result = $gsent->fetchAll();

  return $result;
}

function getUserResources($userId)
{

  $query = 'SELECT * FROM user_resource WHERE user_id=?';
  $gsent = conexionCover()->prepare($query);
  $gsent->execute(array($userId));

  $result = $gsent->fetchAll();

  return $result;
}
function getUserCategoriesnoRepeat($userId)
{

  $query = 'SELECT DISTINCT category,user_id FROM user_resource WHERE user_id=?';
  $gsent = conexionCover()->prepare($query);
  $gsent->execute(array($userId));

  $result = $gsent->fetchAll();

  return $result;
}

function loadAllNewstoPage($userId)
{

  $query = 'SELECT * FROM all_news WHERE user_id = ?  order by date DESC limit 30';
  $gsent = conexionCover()->prepare($query);
  $gsent->execute(array($userId));

  $result = $gsent->fetchAll();

  return $result;
}
function loadAllNewstoPageByCategory($userId, $category)
{

  $query = 'SELECT * FROM all_news WHERE user_id = ? and category=? order by date DESC limit 5';
  $gsent = conexionCover()->prepare($query);
  $gsent->execute(array($userId, $category));

  $result = $gsent->fetchAll();

  return $result;
}


function getAllResourcesUrl($userId)
{

  $query = 'SELECT url,category FROM user_resource WHERE user_id = ?';
  $gsent = conexionCover()->prepare($query);
  $gsent->execute(array($userId));

  $result = $gsent->fetchAll();

  return $result;

  var_dump($result);
}


function deleteOldNews($userId)
{
  $query = 'DELETE  FROM all_news WHERE user_id= ?';
  $gsent = conexionCover()->prepare($query);
  $gsent->execute(array($userId));

  $result = $gsent->fetchAll();

  return $result;
}
function convertDateTomySqlFormat($str)
{
  return date('Y-m-d H:i:s', strtotime($str));
}

function insertNews($userId, $source_name, $category, $resource, $title, $description, $permalink, $date, $img_url, $categories)
{
  $query = 'INSERT INTO all_news (user_id,source_name ,category, resource, title, description, permalink, date, img_url, categories) VALUES (?, ?,?, ?, ?, ?, ?, ?, ?, ?);';
  $addSentence = conexionCover()->prepare($query);
  $addSentence->execute(array($userId, $source_name, $category, $resource, $title, $description, $permalink, $date, $img_url, $categories));
}

function loadNewsDataBase($userId)
{

  $urls = getAllResourcesUrl($userId);

  deleteOldNews($userId);

  foreach ($urls as $urlkey) {
    $i = 0;
    $urlResourse =  $urlkey['url'];
    $categoryUser = $urlkey['category'];

    $rss = simplexml_load_file($urlResourse);


    // foreach ($rss->channel->item as $item) {

    //$media_content = $item->children($namespaces['media']);

    //  foreach ($media_content->content->thumbnail as $i) {
    //   var_dump((string)$i->attributes()->url);
    //   echo $i;
    // }


    // $namespaces = $rss->getNamespaces(true);
    // foreach ($rss->channel->item as $item) {
    //   $media_content =$item->children($namespaces['media']);

    //   foreach ($media_content->content->thumbnail as $i) {
    //     var_dump((string)$i->attributes()->url);
    //     echo $i;
    //   }

    //   echo 'asd';
    // }


    foreach ($rss->channel as $channel) {
      $source_name = $channel->title;
    }
    $nume = 0;
    foreach ($rss->channel->item as $item) {
      $link = $item->link;  //extrae el Permanklink
      $title = $item->title;  //extrae el titulo
      $date = $item->pubDate;  //extrae la fecha

      $url_imagen = "";



        $namespaces = $item->getNamespaces(true);
  
        $media_content = $item->children($namespaces['media']);
  
        foreach ($media_content->content->thumbnail as $i) {
  
          $r = $i->attributes()->url;
  
          $url_imagen = $r;
        }
  
        

      if (!$url_imagen) {
        $url_imagen = $item->enclosure['url']; //extrae el link de la imagen 
      }
      if (!$url_imagen) {
        $url_imagen = $item->children('media', true)->content->attributes(); //extrae link de imagen
      }

      if (!$url_imagen) {
        $url_imagen = "http://www.staticwhich.co.uk/static/images/products/no-image/no-image-available.png";
      }



      $categoryNew = $item->category; //extrae categoria
      $guid = $item->guid; //extrae LINK noticia original categoria
      $dateMySqlFormat = convertDateTomySqlFormat($date); // convierte la fecha al formato mySql
      $description = strip_tags($item->description);
      /*     echo $dateMySqlFormat;
                echo $date; */
      $description = strip_tags($item->description);  //extrae la descripcion
      if (strlen($description) > 200) { //limita la descripcion a 400 caracteres
        $stringCut = substr($description, 0, 200);
        $description = substr($stringCut, 0, strrpos($stringCut, ' ')) . '...';
      }
      insertNews($userId, $source_name, $categoryUser, $urlResourse, $title, $description, $guid, $dateMySqlFormat, $url_imagen, $categoryNew);




      /*   if ($i < 16) { // extrae solo 16 items
                    echo '<div class="cuadros1"><h4><a href="' . $link . '" target="_blank">' . $title . '</a></h4><br><img src="' . $url_imagen . '"><br>' . $description . '<br><div class="time">' . $date . '</div></div>';
                }
                $i++;  */
    }
  }

  return $urls;
}

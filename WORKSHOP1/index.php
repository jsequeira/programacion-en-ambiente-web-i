<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous" />
</head>

<body>



    <div class="container mt-5">
        <div class="row">
            <div class="col-md-6">
                <div class="alert-primary" id="1">

                    <?php

                    function setInterval($milliseconds)
                    {
                        $seconds = (int)$milliseconds / 1000;
                        while (true) {

                            include 'date.php';
                            sleep($seconds);
                        }
                    }
                    setInterval(1000)
                    ?>

                </div>
            </div>
        </div>
    </div>

</body>

</html>
<?php

include_once 'funciones.php';

$carreras = obtenerCarreras();

if ($_GET) {

    $idCarrera = $_GET['carrera'];
    $cedula = $_GET['cedula'];
    $nombre = $_GET['nombre'];
    $apellido = $_GET['apellido'];
    $correo = $_GET['correo'];
    $fecha = $_GET['fecha'];

    $sql_agregar = 'INSERT INTO matricula(id_carrera,cedula,nombre,apellido,correo,fecha) VALUES (?,?,?,?,?,?)';
    $sentencia_agregar = $pdo->prepare($sql_agregar);
    $sentencia_agregar->execute(array($idCarrera, $cedula, $nombre, $apellido, $correo, $fecha));
    echo 'agregado';
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width= , initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous" />
    <title>Document</title>
    <style>
        main {
            height: 50vh;
            background-color: lightblue;
        }
    </style>

</head>

<body>
    <main class="container d-flex align-items-center justify-content-center">
        <div class="row">
            <div class="col text-center ">
                <form method="GET" class="text-center ">
                    <select class="form-select" name="carrera">
                        <?php foreach ($carreras  as $dato) : ?>

                            <option value="<?php echo $dato['id'] ?>">

                                <?php echo $dato['nombre_carrera'] ?>

                            </option>

                        <?php endforeach ?>

                    </select>
                    <input type="text" class="form-control" name="cedula" placeholder="cedula" />
                    <input type="text" class="form-control" name="nombre" placeholder="Nombre" />
                    <input type="text" class="form-control" name="apellido" placeholder="Apellido" />
                    <input type="text" class="form-control" name="correo" placeholder="Correo" />
                    <input type="text" class="form-control" name="fecha" value="<?php $fecha = getdate();
                                                                                echo $fecha['mday'], '/', $fecha['mon'], '/', $fecha['year']; ?>" hidden />
                    <a href="matricular.php" class="btn btn-primary  mt-3">Registrar</a>

                </form>
                <a href="index.php?accion=Desconectar" class="btn btn-primary  mt-3">desconectar</a>
            </div>
        </div>

    </main>
</body>

</html>
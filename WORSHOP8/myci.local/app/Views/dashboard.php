<?php



$userController=	$userModel = new \App\Controllers\User();

$allUser=$userController->getAllUsers();

var_dump($allUser);

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Document</title>

  <link rel="stylesheet" href="dashboard.css" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" />
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
  <!-- MDB -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.css" rel="stylesheet" />
</head>

<body>

  <!-- ----------------------------------------------------------------------- -->
  <!--                                 HEADER                                  -->
  <!-- ----------------------------------------------------------------------- -->

  <header>
  <nav class="navbar navbar-expand-md navbar-light bg-light border-bottom">
    <a class="navbar-brand" href="index.php">
      <img src="img/logo2.png" width="130" height="70" class="d-inline-block align-top" alt="" />
    </a>
    <div class="collapse navbar-collapse justify-content-end " id="navbarNav">
      <ul class="navbar-nav me-5">

        <div class="btn-group me-4">
          <button type="button" class="btn btn-outline-secondary  dropdown-toggle" data-mdb-toggle="dropdown" data-mdb-display="static" aria-expanded="false">
           
          </button>
          <ul class="dropdown-menu dropdown-menu-end dropdown-menu-lg-start ">
            <li><a class="dropdown-item text-center " href="newresource.php">New Resource</a></li>
            <li><a class="dropdown-item text-center " href="index.php?action=logout">Log Out</a></li>

          </ul>

        </div>

      </ul>
    </div>
  </nav>
  </header>

  <!-- ----------------------------------------------------------------------- -->
  <!--                                  MAIN                                   -->
  <!-- ----------------------------------------------------------------------- -->

  <main>

    <div class="container-scroll  container  d-flex flex-column  align-items-center  border-secondary ">

      <div data-mdb-spy="scroll" data-mdb-target="#scrollspy1" data-mdb-offset="0" class="scrollspy-example">
        <table class="table align-middle border  border-secondary">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Nombre</th>
              <th scope="col">Editar</th>
              <th scope="col">Eliminar</th>
            </tr>
          </thead>
          <tbody>
            <?php
            foreach ($allUser as $user) :
            ?>
              <tr>
                <th scope="row">2</th>
                <td><?php echo $user->usuario ?></td>

                <td>
                  <a href="editaction/<?php echo $user->usuario ?>">
                    <button type="button" class="btn btn-success btn-sm px-3">
                      <i class="fas fa-edit"></i>
                    </button>
                  </a>
                </td>
                <td>
                  <a href="delete/<?php echo $user->usuario ?> ">
                    <button  type="button" class="btn btn-danger btn-sm px-3">
                      <i class="fas fa-eraser"></i>
                    </button>
                  </a>
                </td>
              </tr>


            <?php
            endforeach
            ?>

          </tbody>
        </table>
      </div>
      <div class="container w-75 mt-4 ms-5">
        <a href="addcategory.php"> <button type="button" class="btn btn-primary  ">
            Agregar
          </button>
        </a>
    
      </div>

    </div>

  </main>

  <!-- ----------------------------------------------------------------------- -->
  <!--                                 FOOTER                                  -->
  <!-- ----------------------------------------------------------------------- -->

  <footer class="bg-light text-center text-lg-start">
    <!-- Grid container -->
    <div class="container p-4">
      <!--Grid row-->
      <div class="row">
        <!--Grid column-->
        <div class="col-lg-6 col-md-12 mb-4 mb-md-0">
          <h5 class="text-uppercase">About</h5>

          <p>
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Iste atque
            ea quis molestias. Fugiat pariatur maxime quis culpa corporis vitae
            repudiandae aliquam voluptatem veniam, est atque cumque eum delectus
            sint!
          </p>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
          <h5 class="text-uppercase">Devices</h5>

          <ul class="list-unstyled mb-0">
            <li>
              <a href="#!" class="text-dark">PC</a>
            </li>
            <li>
              <a href="#!" class="text-dark">iOS</a>
            </li>
            <li>
              <a href="#!" class="text-dark">Android</a>
            </li>

          </ul>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
          <h5 class="text-uppercase mb-0">Social media</h5>

          <ul class="list-unstyled">
            <li>
              <a href="#!" class="text-dark">Fcebook</a>
            </li>
            <li>
              <a href="#!" class="text-dark">twitter</a>
            </li>
            <li>
              <a href="#!" class="text-dark">Diaspora</a>
            </li>
          </ul>
        </div>
        <!--Grid column-->
      </div>
      <!--Grid row-->
    </div>
    <!-- Grid container -->

    <!-- Copyright -->
    <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2)">
      © 2020 Copyright:
      <a class="text-dark" href="https://mdbootstrap.com/">MDBootstrap.com</a>
    </div>
    <!-- Copyright -->
  </footer>
  <!-- MDB -->

  <!-- ----------------------------------------------------------------------- -->
  <!--                               JAVASCRIPH                                -->
  <!-- ----------------------------------------------------------------------- -->

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.js"></script>

  <!-- ----------------------------------------------------------------------- -->
  <!--                               JAVASCRIPH                                -->
  <!-- ----------------------------------------------------------------------- -->

</body>

</html>